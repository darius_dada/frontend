// var API_URL = "http://localhost:3000";
var API_URL = "https://yrkark5lf2.execute-api.eu-west-1.amazonaws.com/int"

// ################################################
// common functions
// ################################################

function loadImages(callBackFunction) {
  connection_settings = {
    url: API_URL + "/dog",
    type: "GET",
    contentType: "application/json",
    success: callBackFunction
  };
  $.ajax(connection_settings)
}


// ################################################
// admin page functions
// ################################################


function uploadImage() {
  var file = $("#photoupload")[0].files[0];
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    var dataToUpload = new Object();
    dataToUpload.image = reader.result;
    dataToUpload.description = $.trim($('#description').val());
    dataToUpload.dog_name = $.trim($('#dogname').val());
    dataToUpload.image_name = file.name;

    connection_settings = {
      url: API_URL + "/dog",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(dataToUpload),
      success: function () {
        alert('Inregistrarea a fost adaugata cu succes!');
        $('#addDogForm')[0].reset();
        loadImages(buildAdminGallery);
      }
    };
    $.ajax(connection_settings)
  };
}

function buildAdminGallery(response) {
  $('#galerie').empty();
  $.each(response, function () {
    $('#galerie').append(generateAdminImageDiv(this));
  })
}

function generateAdminImageDiv(dogData) {
  var dogDetailsDiv = document.createElement('div')
  $(dogDetailsDiv).addClass(['card', 'ml-2', 'mt-2'])
  dogDetailsDiv.style = 'width: 16rem;'

  var imageThumbnail = document.createElement('img');
  imageThumbnail.src = dogData.image_url;
  $(imageThumbnail).addClass('card-img-top');
  $(dogDetailsDiv).append(imageThumbnail);

  var cardDetailsDiv = document.createElement('div')
  $(cardDetailsDiv).addClass('card-body')

  var cardTitle = document.createElement('h5');
  $(cardTitle).addClass('card-title');
  $(cardTitle).text(dogData.dog_name);

  var cardDesc = document.createElement('p');
  $(cardDesc).addClass('card-text');
  $(cardDesc).text(dogData.description);

  var editBtn = document.createElement('div');
  $(editBtn).addClass(['btn btn-warning']);
  $(editBtn).text("Editare");

  editBtn.setAttribute('data-toggle', 'modal');
  editBtn.setAttribute('data-target', '#editModal');
  editBtn.onclick = function () {
    $('#editDogForm')[0].reset();
    $('#editModalLabel').text('Editare ' + dogData.dog_name);
    $('#editdogname').val(dogData.dog_name);
    $('#editdescription').val(dogData.description);
    $('#editdogID').val(dogData.ID);
  }

  var deleteBtn = document.createElement('div');
  $(deleteBtn).text('Stergere!');
  $(deleteBtn).addClass(['btn btn-danger']);
  deleteBtn.onclick = function () {
    deleteDog(dogData.ID);
  }

  //add card details
  $(dogDetailsDiv).append(cardTitle);
  $(dogDetailsDiv).append(cardDesc);
  $(dogDetailsDiv).append(editBtn);
  $(dogDetailsDiv).append(document.createElement('br'))
  $(dogDetailsDiv).append(deleteBtn);

  $(dogDetailsDiv).append(cardDetailsDiv);

  return dogDetailsDiv
}

function deleteDog(id) {
  let confirmation = confirm("Apasati OK pentru a sterge cainele din baza de date!");
  if (!confirmation)
    return
  connection_settings = {
    url: API_URL + "/dog/" + id,
    type: "DELETE",
    contentType: "application/json",
    success: function () {
      loadImages(buildAdminGallery);
    }
  };
  $.ajax(connection_settings)
}


function editDog() {
  console.log("Edit dog function called for: " + $('#editdogID').val());
  var dataToUpload = new Object();
  dataToUpload.description = $.trim($('#editdescription').val());
  dataToUpload.dog_name = $.trim($('#editdogname').val());

  connection_settings = {
    url: API_URL + "/dog/" + $.trim($('#editdogID').val()),
    type: "PATCH",
    contentType: "application/json",
    data: JSON.stringify(dataToUpload),
    success: function () {
      alert('Modificarile au fost salvate cu succes!');
      $('#editModalLabel').text('Editare ' + $.trim($('#editdogname').val()));
      loadImages(buildAdminGallery);
    }
  }
  $.ajax(connection_settings)
}

// ################################################
// main page functions
// ################################################

function buildGaleryCarousel(response) {
  var i = 0;
  var galleryDiv = document.createElement('div')
  galleryDiv.id = 'gallery';
  $(galleryDiv).addClass('row')
  galleryDiv.setAttribute('data-toggle', 'modal');
  galleryDiv.setAttribute('data-target', '#galeryModal');
  var i = 0;

  var carouselIdicatorsList = document.createElement('ol');
  $(carouselIdicatorsList).addClass('carousel-indicators');

  var carouselInnerDiv = document.createElement('div');
  $(carouselInnerDiv).addClass('carousel-inner');

  $.each(response, function () {
    galleryDiv.append(generateGalleryImageDiv(this, i));
    carouselIdicatorsList.append(generateCarouselListElement(i));
    carouselInnerDiv.append(generateCarouselItemDiv(this, i));
    i++;
  })


  // attach gallery div
  $('#galerie').append(galleryDiv);
  $('#galeryCarousel').append(carouselIdicatorsList);
  $('#galeryCarousel').append(carouselInnerDiv);

}

function generateGalleryImageDiv(imageData, index) {
  var imageDiv = document.createElement('div');
  $(imageDiv).addClass(['col-12', 'col-sm-6', 'col-lg-3']);

  var imgElement = document.createElement('img');
  imgElement.src = imageData.image_url;
  imgElement.setAttribute('data-target', "#galeryCarousel");
  imgElement.setAttribute('data-slide-to', index);
  $(imgElement).addClass('w-100');

  var nameHeader = document.createElement('h5');
  $(nameHeader).text(imageData.dog_name);

  imageDiv.append(imgElement);
  imageDiv.append(nameHeader);

  return imageDiv
}

function generateCarouselListElement(index) {
  var listItem = document.createElement('li');
  if (index == 0)
    $(listItem).addClass('active');
  listItem.setAttribute('data-target', '#galeryCarousel');
  listItem.setAttribute('data-slide-to', index);
  return listItem

}

function generateCarouselItemDiv(dogData, index) {
  var carouselItemDiv = document.createElement('div');
  $(carouselItemDiv).addClass('carousel-item');
  if (index == 0)
    $(carouselItemDiv).addClass('active')


  var imgElement = document.createElement('img');
  imgElement.src = dogData.image_url;
  $(imgElement).addClass(['d-block', 'w-100']);

  var detailsDiv = document.createElement('div');

  var cardTitle = document.createElement('h5');
  $(cardTitle).addClass('card-title');
  $(cardTitle).text(dogData.dog_name);
  $(detailsDiv).append(cardTitle);

  var descriptionParaghaph = document.createElement('p');
  $(descriptionParaghaph).addClass('card-text');
  $(descriptionParaghaph).text(dogData.description);
  $(detailsDiv).append(descriptionParaghaph);

  // var addoptMeButton = document.createElement('a');
  // $(addoptMeButton).addClass(['btn', 'btn-primary']);
  // $(addoptMeButton).text('Ia-ma acasa!');
  // addoptMeButton.onclick = sendAdoptionEmail(dogData.ID);
  // $(detailsDiv).append(addoptMeButton);

  $(carouselItemDiv).append(imgElement);
  $(carouselItemDiv).append(detailsDiv);

  return carouselItemDiv
}


function sendAdoptionEmail(dogId) {
  //todo  - implement functionality
  console.log('To send email for dog: ' + dogId);
}


